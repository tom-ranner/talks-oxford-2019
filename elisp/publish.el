(package-initialize)
(require 'org)
(require 'ox-publish)

(add-to-list 'load-path
	     (expand-file-name
	      "../org-reveal/" (file-name-directory load-file-name)))
(require 'ox-reveal)

(find-file "celegans.org")
(org-reveal-export-to-html)

(provide 'publish)
;;; publish.el ends here
